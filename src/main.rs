extern crate byteorder;
extern crate cairo;
extern crate gio;
extern crate gtk;
extern crate libpulse_binding as pulse;
extern crate rand;

use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;
use std::sync::mpsc;

use cairo::{Antialias, Context};
use gio::prelude::*;
use gtk::{Application, ApplicationWindow, DrawingArea};
use gtk::prelude::*;
use std::thread;
use std::sync::mpsc::Receiver;
use crate::audio::ProcessedSample;

mod audio;

struct WaveformData(VecDeque<ProcessedSample>);

impl WaveformData {
    fn new() -> WaveformData {
        WaveformData(VecDeque::new())
    }

    fn import(&mut self, rx: &Receiver<ProcessedSample>) {
        loop {
            if let Ok(sample) = rx.try_recv() {
                self.0.push_back(sample);
                if self.0.len() > 300 {
                    self.0.pop_front();
                }
            } else {
                break;
            }
        }
    }

    fn draw(&self, y_offset: f64, cairo: &Context) {
        for (i, sample) in self.0.iter().rev().enumerate() {
            let scale_ratio = 40 as f64 / (u16::max_value() / 2) as f64;
            let positive = sample.positive_peak as f64 * scale_ratio;
            let negative = sample.negative_peak as f64 * scale_ratio;
            cairo.move_to((300 - i) as f64, y_offset + 50 as f64 - positive);
            cairo.line_to((300 - i) as f64, y_offset + 50 as f64 + negative);
        }
    }
}

fn main() {
    let (speaker_tx, speaker_rx) = mpsc::channel();
    let (mic_tx, mic_rx) = mpsc::channel();
    thread::spawn(move || {
        audio::audio(speaker_tx, mic_tx);
    });
    ui(speaker_rx, mic_rx);
}

fn ui(speaker_rx: Receiver<ProcessedSample>, mic_rx: Receiver<ProcessedSample>) {
    let speaker_rx = Rc::new(RefCell::new(speaker_rx));
    let mic_rx = Rc::new(RefCell::new(mic_rx));
    let application = Application::new(Some("com.github.gtk-rs.examples.basic"), Default::default())
        .expect("failed to initialize GTK application");

    application.connect_activate(move |app| {
        let speaker_rx = speaker_rx.clone();
        let mic_rx = mic_rx.clone();
        let window = ApplicationWindow::new(app);
        window.set_title("Pulsetheatre");
        window.set_default_size(300, 200);

        let speaker_data = Rc::new(RefCell::new(WaveformData::new()));
        let mic_data = Rc::new(RefCell::new(WaveformData::new()));
        let drawing_area = DrawingArea::new();
        {
            let speaker_data = speaker_data.clone();
            let mic_data = mic_data.clone();
            drawing_area.connect_draw(move |_, cairo| {
                cairo.set_antialias(Antialias::None);
                cairo.set_source_rgb(0f64, 0f64, 0f64);
                cairo.set_line_width(1f64);

                cairo.move_to(0 as f64, 100 as f64);
                cairo.line_to(300 as f64, 100 as f64);

                speaker_data.borrow().draw(0 as f64, &cairo);
                mic_data.borrow().draw(100 as f64, &cairo);
                cairo.stroke();
                Inhibit(false)
            });
        }
        drawing_area.add_tick_callback(move |drawing_area, _| {
            let mut speaker_data = speaker_data.borrow_mut();
            let speaker_rx = speaker_rx.borrow();
            speaker_data.import(&speaker_rx);

            let mut mic_data = mic_data.borrow_mut();
            let mic_rx = mic_rx.borrow_mut();
            mic_data.import(&mic_rx);

            drawing_area.queue_draw();
            Continue(true)
        });
        window.add(&drawing_area);

        window.show_all();
    });

    application.run(&[]);
}