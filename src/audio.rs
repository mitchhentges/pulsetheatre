use std::fmt;
use std::cell::RefCell;
use std::error::Error;
use std::fmt::Formatter;
use std::io::Cursor;
use std::rc::Rc;

use byteorder::{LittleEndian, ReadBytesExt};
use pulse::callbacks::ListResult;
use pulse::context;
use pulse::context::Context;
use pulse::context::flags::NOAUTOSPAWN;
use pulse::error::PAErr;
use pulse::mainloop::standard::{IterateResult, Mainloop};
use pulse::sample::{Format, Spec};
use pulse::stream::{PeekResult, Stream};
use pulse::stream::flags::NOFLAGS;
use std::sync::mpsc::Sender;
use std::time::SystemTime;

const DEFAULT_SAMPLE_SPEC: Spec = Spec {
    channels: 1,
    rate: 44100,
    format: Format::S16le,
};
const SAMPLE_LEN: usize = 44100 / 120;
const LOUD_THRESHOLD: i16 = 30000;

#[derive(Debug)]
pub struct ProcessedSample {
    pub positive_peak: u16,
    pub negative_peak: u16,
}

impl ProcessedSample {
    fn from_sample(series: &[i16; SAMPLE_LEN]) -> ProcessedSample {
        let mut positive_peak: u16 = 0;
        let mut negative_peak: u16 = 0;

        for &i in series.iter() {
            if i > 0 && i as u16 > positive_peak {
                positive_peak = i as u16;
            }
            if i < 0 {
                let negative_i = if i == i16::min_value() {
                    u16::max_value() / 2
                } else {
                    -i as u16
                };
                if negative_i > negative_peak {
                    negative_peak = negative_i;
                }
            }
        }
        return ProcessedSample {
            positive_peak,
            negative_peak
        }
    }
}

fn listen(tx: Sender<ProcessedSample>, context: &mut Context, monitor_name: &str, start: SystemTime) {
    dbg!(monitor_name);
    let stream = Stream::new(context, "pulsetheatre", &DEFAULT_SAMPLE_SPEC, None).unwrap();
    let stream_rc = Rc::new(RefCell::new(stream));
    let stream = stream_rc.clone();
    let mut stream = stream.borrow_mut();

    let mut series = [0; SAMPLE_LEN];
    let mut series_index = 0;

    let mut request_confirmed = false;

    let monitor_name = monitor_name.to_string();
    stream.connect_record(Some(&monitor_name), None, NOFLAGS).unwrap();
    stream.set_read_callback(Some(Box::new(move |_| {
        let mut stream = stream_rc.borrow_mut();
        match stream.peek() {
            Ok(data) => {
                match data {
                    PeekResult::Empty => {
                        println!("empty");
                    },
                    PeekResult::Hole(_) => {
                        println!("hole detected");
                    },
                    PeekResult::Data(data) => {
                        let mut i16_chunks = vec![0; data.len() / 2];
                        Cursor::new(data).read_i16_into::<LittleEndian>(&mut i16_chunks).unwrap();
                        for chunk in i16_chunks {
                            if chunk > LOUD_THRESHOLD && !request_confirmed {
                                let ms = SystemTime::now()
                                    .duration_since(start)
                                    .unwrap()
                                    .as_millis();
                                println!("[{}] {} is loud", ms, monitor_name);
                                request_confirmed = true;
                            }

                            series[series_index] = chunk;
                            series_index += 1;
                            if series_index == series.len() {
                                series_index = 0;
                                tx.send(ProcessedSample::from_sample(&series)).unwrap();
                            }
                        }

                    }
                };
            },
            Err(_) => {
                panic!("Errored in set_read_callback");
            }
        }
        stream.discard().unwrap();
    })));
}


fn init(speaker_tx: Sender<ProcessedSample>, mic_tx: Sender<ProcessedSample>, context: Rc<RefCell<Context>>) {
    let context = context.clone();
    let introspector = context.borrow().introspect();
    introspector.get_server_info(move |info| {
        let context = context.clone();
        let speaker_tx = speaker_tx.clone();
        let start = SystemTime::UNIX_EPOCH;

        listen(
            mic_tx.clone(),
            &mut *context.borrow_mut(),
            &info.default_source_name.clone().unwrap(),
            start.clone(),
        );

        let introspector = context.borrow().introspect();
        introspector.get_sink_info_by_name(
            &info.default_sink_name.clone().unwrap(),
            move |list_result| {
                if let ListResult::Item(sink_info) = list_result {
                    let monitor_name = sink_info.monitor_source_name.clone().unwrap();
                    listen(speaker_tx.clone(), &mut *context.borrow_mut(), &monitor_name, start)
                }
            });
    });
}

pub fn audio(speaker_tx: Sender<ProcessedSample>, mic_tx: Sender<ProcessedSample>) {
    let mut mainloop = Mainloop::new().unwrap();
    let mut context = Context::new(&mainloop, "pulsetheatre").unwrap();
    context.connect(Some("/run/user/1000/pulse/native"), NOAUTOSPAWN, None).unwrap();
    context.wait(&mut mainloop).unwrap();

    let context = Rc::new(RefCell::new(context));
    init(speaker_tx, mic_tx, context.clone());
    mainloop.run().unwrap();
}

#[derive(Debug)]
enum WaitError {
    Failed(Option<PAErr>),
    Cancelled,
}

impl Error for WaitError {}

impl fmt::Display for WaitError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), ::std::fmt::Error> {
        match *self {
            WaitError::Failed(_) => f.write_str("Failed"),
            WaitError::Cancelled => f.write_str("Cancelled"),
        }
    }
}

trait WaitableOperation {
    fn wait(&self, mainloop: &mut Mainloop) -> Result<(), WaitError>;
}

impl WaitableOperation for Context {
    fn wait(&self, mainloop: &mut Mainloop) -> Result<(), WaitError> {
        loop {
            match mainloop.iterate(false) {
                IterateResult::Quit(_) => {
                    return Err(WaitError::Cancelled);
                }
                IterateResult::Err(err) => {
                    return Err(WaitError::Failed(Some(err)));
                }
                IterateResult::Success(_) => {}
            };
            match self.get_state() {
                context::State::Ready => { break; }
                context::State::Failed |
                context::State::Terminated => {
                    return Err(WaitError::Failed(None));
                }
                _ => {}
            }
        }
        Ok(())
    }
}